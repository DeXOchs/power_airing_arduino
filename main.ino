#include <Arduino_APDS9960.h>

// LED color pins
#define BLUE 24
#define GREEN 23
#define RED 22

// pins
#define ALERT_PIN A7
#define PWM_PIN D10

// timer constants
#define WINDOW_OPEN_TIMER 480000 // 8min
#define ROOM_AIR_QUALITY_TIMER 60000 // 1min
#define ALERT_FIRED_TIMER 10000 // alert every 10sec

int state = 0; // closed window
int lastWindowOpened = 0;

void setup() {
  pinMode(BLUE, OUTPUT);
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  
  Serial.begin(9600);
  pinMode(PWM_PIN, INPUT);

  if (!APDS.begin()) {
    Serial.println("Error initializing APDS-9960 sensor!");
  }

  turnLEDOff();
  welcome();
}

void loop() {

  switch (state) {
    case 0: // window closed
      // check window
      if (isWindowOpen()) {
        toWindowOpenedState();
      } else {
        // signal room air quality
        signalRoomAirQuality();
      }
      break;
      
    case 1: // window opened
      // check window
      if (!isWindowOpen()) {
        toWindowClosedState();
      } else if (millis() - lastWindowOpened > WINDOW_OPEN_TIMER) {
        toAlertState();
      }
      break;
      
    case 2: // alert
      // check window
      if (!isWindowOpen()) {
        toWindowClosedState();
      } else {
        // alert now
        alert(true);
      }
      break;
      
    default:
      Serial.println("Undefined state not in [0;1;2]");
  }
  
  delay(100); // checker interval
}

// transitions

int lastRoomAirQualitySignal = 0;
void toWindowClosedState() {
  Serial.println("window closed");
  state = 0;
  lastRoomAirQualitySignal = millis();
  turnLEDOff();
}

void toWindowOpenedState() {
  Serial.println("window opened");
  state = 1;
  lastWindowOpened = millis();
  turnLEDOff();
  alert(false);
}

int lastAlertFired = 0;
void toAlertState() {
  Serial.println("timer elapsed");
  state = 2;
  lastAlertFired = millis() + ALERT_FIRED_TIMER + 1; // fire alert immediately 
}


// functional methods

void welcome() {
  int melody[17][2] = {
    {784, 250},
    {932, 250},
    {1047, 333},
    {784, 125},
    {0, 125},
    {932, 125},
    {0, 125},
    {1109, 125},
    {1047, 500},
    {0, 50},
    {784, 250},
    {932, 250},
    {1047, 333},
    {0, 50},
    {932, 125},
    {0, 125},
    {784, 500}
  };
  for (int i = 0; i < 17; i++) {
    tone(ALERT_PIN, melody[i][0]);
    delay(melody[i][1]);
    noTone(ALERT_PIN);
  }
}

int lastProximityValue = 0;
bool isWindowOpen() {
  if (APDS.proximityAvailable()) {
    lastProximityValue = APDS.readProximity();
  }
  //Serial.println(lastProximityValue);
  return lastProximityValue > 0;
}

void signalRoomAirQuality() {
  if (millis() - lastRoomAirQualitySignal > ROOM_AIR_QUALITY_TIMER) {
    int th = int(pulseIn(PWM_PIN, HIGH, 1004000) / 1000);
    int tl = 1004 - th;
    int co2_ppm = 2000 * (th - 2) / (th + tl - 4);
    turnLEDOff();
    if (co2_ppm < 0) {
      // sensor not ready or fault
    } else if (co2_ppm < 1000) {
      setGreen(true);
    } else if (co2_ppm > 2000) {
      setRed(true);
    } else {
      setGreen(true);
      setRed(true);
    }
    lastRoomAirQualitySignal = millis();
  }
}

void alert(bool loudly) {
  if (millis() - lastAlertFired > ALERT_FIRED_TIMER) {
    for (int i = 0; i < 5; i++) {
      if (loudly) tone(ALERT_PIN, 2000);
      setBlue(true);
      
      delay(100);
      
      noTone(ALERT_PIN);
      setBlue(false);
      
      delay(100);
    }
    lastAlertFired = millis();
  }
}

// LED
void setBlue(bool show) {
  if (show) digitalWrite(BLUE, LOW);
  else digitalWrite(BLUE, HIGH);
}
void setRed(bool show) {
  if (show) digitalWrite(RED, LOW);
  else digitalWrite(RED, HIGH);
}
void setGreen(bool show) {
  if (show) digitalWrite(GREEN, LOW);
  else digitalWrite(GREEN, HIGH);
}
void turnLEDOff() {
  setBlue(false);
  setRed(false);
  setGreen(false);
}

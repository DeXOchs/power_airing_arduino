# Function
A state machine with 3 states:
1. Closed Window (proximity sensor recognizes sth. near by) – show co2 concentration goodness with 3 LED colors red, orange, green
2. Opened Window (proximity sensor doesn't recognizes sth. near by) – a timer of 8 minutes starts
3. Alert – timer elapsed and is shown as blue flashing LED paired with alert sounds

# Used Components
- Arduino Nano 33 BLE Sense (LED, Prox. Sensor)
- Winsen MH-Z16 CO2-Sensor
- simple mainboard buzzer

# Context
Developed for Ubuquitous Computing lecture, Prof. Dr. M. Beigl, TECO, KIT
